//create a program that's going to store our news feed preferences

//define our constants
const input = require('readline-sync');

//define our functions

//decide data structure(s)

let userOne = {
  firstName: "Matt",
  lastName: "Moore",
  userName: "mm00re",
  password: "12345",
  age: "none of your business",
  newsSources: ["NPR", "AP", "BBC", "CNN"]
}

//define my program

let gatherUserName = input.question("What is your username");

let gatherPassWord = input.question("What is your password");

if (gatherUserName == userOne.userName && gatherPassWord == userOne.password) {
  console.log("You're logged in");
} else {
  console.log("error");
}
